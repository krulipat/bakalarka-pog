using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MissionExit : MonoBehaviour
{
    [SerializeField] private Trigger trigger;
    [SerializeField] private string missionName;
    [SerializeField] private string branikSceneName = "0-branik";

    private void Start() {
        trigger.activationEvent.AddListener(ChangeScene);
    }

    public void ChangeScene() {
        GameObject gameStateGO = GameObject.FindGameObjectWithTag("GameState");
        GameState gameState = gameStateGO.GetComponent<GameState>();
        if (gameState == null) {
            Debug.LogError("No game state found.");
            return;
        }

        MissionsState ms = gameState.GetMissionsState();
        ms.MarkCompleted(missionName);
        SceneManager.LoadScene(branikSceneName);

    }
}
