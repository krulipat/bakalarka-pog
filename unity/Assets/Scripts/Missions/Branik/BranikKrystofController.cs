using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BranikKrystofController : MonoBehaviour
{
    [SerializeField] private DialogueSO afterUneticeDialogue;
    [SerializeField] private DialogueSO starobrnoCompletedFirstDialogue;
    [SerializeField] private DialogueSO staropramenCompletedFirstDialogue;
    [SerializeField] private DialogueSO beforePlzenDialogue;
    [SerializeField] private DialogueSO afterPlzenDialogue;
    [SerializeField] private Trigger dialogueTrigger;
    [SerializeField] private BranikExitController exitController;
    [SerializeField] private Animator animator;

    private MissionsState missionState;
    private DialogueUI dialogueUI;

    private void Start() {
        dialogueTrigger.activationEvent.AddListener(ChooseDialogue);
        missionState = GameObject.FindGameObjectWithTag("GameState").GetComponent<GameState>().GetMissionsState();
        // TODO change to not so dirty solution
        dialogueUI = GameObject.FindAnyObjectByType<DialogueUI>();
        animator.SetBool("sitting", true);
    }



    private void ChooseDialogue() {
        if (missionState.GetMostRecentMission() == "unetice") {
            PlayDialogue(afterUneticeDialogue);
        } else if (!missionState.IsCompleted("starobrno") && missionState.IsCompleted("staropramen")) {
            PlayDialogue(staropramenCompletedFirstDialogue);
            exitController.SetupExit("starobrno");
        } else if (missionState.IsCompleted("starobrno") && !missionState.IsCompleted("staropramen")) {
            PlayDialogue(starobrnoCompletedFirstDialogue);
            exitController.SetupExit("staropramen");
        } else if (missionState.IsAvailable("plzen")) {
            PlayDialogue(beforePlzenDialogue);
            exitController.SetupExit("plzen");
        } else if (missionState.IsCompleted("plzen")) {
            PlayDialogue(afterPlzenDialogue);
        }
    }

    private void PlayDialogue(DialogueSO dialogue) {
        dialogueTrigger.SetIsEnabled(false);
        dialogueUI.dialogueEndEvent.AddListener(EndDialogue);
        if (!dialogueUI.StartDialogue(dialogue)) {
            EndDialogue("");
        }
    }

    public void EndDialogue(string endChoice) {
        dialogueUI.dialogueEndEvent.RemoveListener(EndDialogue);
        dialogueTrigger.SetIsEnabled(true);

        switch (endChoice) {
            case "starobrnoFirst":
                exitController.SetupExit("starobrno");
                break;
            case "staropramenFirst":
                exitController.SetupExit("staropramen");
                break;
            case "exit":
                SceneManager.LoadScene("Credits");
                break;
        }    
    }

}
