using UnityEngine;

public class DragonAnimatorOutput : MonoBehaviour {

    [SerializeField] private DamageProducer attack;

    public void Attack() {
        attack.Attack();
    }

}