﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(EventController))]
public class DragonController : DamageableObject {

    [SerializeField] private int maxLives = 25;
    private int lives;
    [SerializeField] private Animator animator;

    EventController eventController;
    public UnityEvent<int, int> livesChangeEvent = new();

    private Coroutine stateChanging;
    // so the dragon doesnt take damage
    private DragonState state = DragonState.standing;


    private void Awake() {
        lives = maxLives;
        eventController = GetComponent<EventController>();  
    }

    private void Start() {
        eventController.eventStart.AddListener(StartFight);
    }

    private void StartFight() {
        ChangeState(DragonState.attacking);
    }

    private void EndFight() {
        eventController.EndEvent();
    }

    public int GetMaxLives() { return maxLives; }

    public int GetLives() { return lives; }

    public override DamageResponseData RecieveDamage(DamageData damageData) {
        Debug.Log("Hit!", gameObject);
        if (state == DragonState.dead || state == DragonState.standing || damageData.damage < 2) {
            return new(isSolid, false);
        }
        lives -= damageData.damage;
        livesChangeEvent.Invoke(lives, maxLives);
        if (lives <= 0) {
            ChangeState(DragonState.dead);
        } else {
            animator.SetTrigger("hit");
        }

        return new(isSolid, false);
    }


    private void EndState() {
        switch (state) {
            case DragonState.sitting:
                if (Random.Range(1, 5) == 1)
                    ChangeState(DragonState.standing);
                else
                    ChangeState(DragonState.attacking);
                break;
            case DragonState.standing:
                ChangeState(DragonState.sitting);
                break;
            case DragonState.attacking:
                ChangeState(DragonState.sitting);
                break;
            case DragonState.dead:
                Invoke(nameof(EndFight), 5f);
                return;
        }
    }

    private IEnumerator EndStateDelayed(float delay) {
        yield return new WaitForSeconds(delay);
        EndState();
    }

    private void ChangeState(DragonState newState) {
        if (stateChanging != null) { 
            StopCoroutine(stateChanging);
        }
        state = newState;
        switch (state) {
            case DragonState.sitting:
                animator.SetBool("IsStanding", false);
                stateChanging = StartCoroutine(EndStateDelayed(2f));
                break;
            case DragonState.standing:
                animator.SetBool("IsStanding", true);
                stateChanging = StartCoroutine(EndStateDelayed(5f));
                break;
            case DragonState.attacking:
                animator.SetTrigger("attack");
                stateChanging = StartCoroutine(EndStateDelayed(2f));
                break;
            case DragonState.dead:
                animator.SetTrigger("death");
                Invoke(nameof(EndFight), 8f);
                break;
        }
    }

    private enum DragonState {
        sitting,
        standing,
        attacking,
        dead
    }
}