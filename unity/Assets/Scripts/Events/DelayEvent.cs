﻿using System.Collections;
using UnityEngine;


[RequireComponent(typeof(EventController))]
public class DelayEvent : MonoBehaviour {

    [SerializeField] private float delay = 0.1f;

    private EventController controller;

    private void Start() {
        controller = GetComponent<EventController>();
        controller.eventStart.AddListener(EndEvent);
    }


    private void EndEvent() {
        StartCoroutine(EndEventCoroutine());
    }

    private IEnumerator EndEventCoroutine() {
        yield return new WaitForSeconds(delay);
        controller.EndEvent();

    }
}
