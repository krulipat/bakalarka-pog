using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PlayerMovement))]

[Serializable]
public class PlayerState{
    public UnityEvent<int, int> livesChangeEvent = new();
    public UnityEvent<int> moneyChangeEvent = new();
    public UnityEvent<int> healthpacksChangeEvent = new();
    public UnityEvent<int> damagePotionsChangeEvent = new();
    public UnityEvent<bool> damagePotionstimerChangeEvent = new();
    public UnityEvent<int> speedPotionsChangeEvent = new();
    public UnityEvent<bool> speedPotionstimerChangeEvent = new();

    private PlayerMovement playerMovement;

    private int maxLives = 10; 
    private int lives = 5; 
    private int money = 100000; 
    private int healthpacks = 3;
    private int damagePotions = 1;
    private int speedPotions = 2;

    public int GetMaxLives() { return maxLives; }

    public int GetLives() { return lives; }

    public int GetMoney() { return money;}

    public int GetHealthpacks() {  return healthpacks; }
    public int GetDamagePotions() { return damagePotions; }

    public int GetSpeedPotions() { return speedPotions; }

    public bool ChangeMoney(int amount) {
        if (money + amount < 0) {
            return false;
        }

        this.money += amount;
        moneyChangeEvent.Invoke(money);
        return true;
    }

    public bool ChangeHealthpacks(int amount) {  
        if (healthpacks + amount < 0) {
            return false;
        }

        this.healthpacks += amount;
        healthpacksChangeEvent.Invoke(healthpacks);
        return true;
    }

    public void ChangeLives(int amount) {
        lives += amount;
        if (lives < 0) {
            lives = 0;
        }
        if (lives > maxLives) {
            lives = maxLives;
        }
        livesChangeEvent.Invoke(lives, maxLives);
    }

    public void AddDamagePotion()
    {
        damagePotions++;
        damagePotionsChangeEvent.Invoke(damagePotions);
    }

    public void AddSpeedPotion()
    {
        speedPotions++;
        speedPotionsChangeEvent.Invoke(speedPotions);
    }

    public void UseHealthpack()
    {
        healthpacks--;
        if (healthpacks < 0)
        {
            healthpacks = 0;
        }
        // heal o 3 �ivoty
        ChangeLives(3);

        healthpacksChangeEvent.Invoke(healthpacks);
    }

    public void UseDamagePotion()
    {
        damagePotions--;
        if (damagePotions < 0)
        {
            damagePotions = 0;
        }
        // aplikuj efekt


        damagePotionsChangeEvent.Invoke(damagePotions);
        damagePotionstimerChangeEvent.Invoke(true);
    }

    public void UseSpeedPotion()
    {
        speedPotions--;
        if (speedPotions < 0)
        {
            speedPotions = 0;
        }

        speedPotionsChangeEvent.Invoke(speedPotions);
        speedPotionstimerChangeEvent.Invoke(true);

    }


    public void SetLives(int lives) {
        this.lives = lives;
        livesChangeEvent.Invoke(lives, maxLives);
    }

    public void SetMoney(int money) { 
        this.money = money;
        moneyChangeEvent.Invoke(money);
    }

}
