using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///     Trigger is activated in the first Update. That means every Start method
///     has been certainly called.
/// </summary>
public class SceneLoadTrigger : Trigger {

    private void Update() {
        Activate();
        Destroy(gameObject);
    }

}
