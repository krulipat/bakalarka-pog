﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    [SerializeField] private Image livesSlider;
    [SerializeField] private DialogueUI dialogueUI;
    [SerializeField] private TextMeshProUGUI livesText;
    [SerializeField] private TextMeshProUGUI moneyText;
    [SerializeField] private TextMeshProUGUI healthpacksText;
    [SerializeField] private TextMeshProUGUI damagePotionText;
    [SerializeField] private TextMeshProUGUI damagePotionTimerText;
    [SerializeField] private TextMeshProUGUI speedPotionText;
    [SerializeField] private TextMeshProUGUI speedPotionTimerText;
    [SerializeField] private GameObject deathScreen;

    private float UIhealth, UImaxHealth;
    private float lerpSpeed;

    private void Start() {
        SetupPlayerDataUI();
    }

    private void Update()
    {
        lerpSpeed = 5f * Time.deltaTime;

        livesSlider.fillAmount = Mathf.Lerp(livesSlider.fillAmount, UIhealth / UImaxHealth, lerpSpeed);
    }

    private void UpdateLivesUI(int health, int maxHealth) {
        livesText.text = $"{health}/{maxHealth} zdraví";

        UIhealth = (float)health;
        UImaxHealth = (float)maxHealth;
    }

    private void UpdateMoneyUI(int money) {
        moneyText.text = $"{money} víček";
    }

    private void UpdateHealthpacksUI(int healthpacks) {
        healthpacksText.text = $"{healthpacks} lékárniček";
    }

    private void UpdatedamagePotionUI(int damagePotions)
    {
        damagePotionText.text = $"{damagePotions} DP";
    }

    private void UpdatespeedPotionUI(int speedPotions)
    {
        speedPotionText.text = $"{speedPotions} SP";
    }

    private void UpdatespeedPotiontimerUI(bool active)
    {
        if (!active)
        {
            speedPotionTimerText.text = $"";
            return;
        }

        StartCoroutine(CountdownspeedPotionTimerUI());
    }

    IEnumerator CountdownspeedPotionTimerUI()
    {
        float currentTime = 15f;

        while (currentTime >= 0)
        {
            int seconds = Mathf.FloorToInt(currentTime);

            speedPotionTimerText.text = $"0:{seconds}";

            yield return new WaitForSeconds(1f); // Čekání na jednu sekundu

            currentTime -= 1f;
        }

        UpdatespeedPotiontimerUI(false);
    }

    private void UpdatedamagePotiontimerUI(bool active)
    {
        if (!active)
        {
            damagePotionTimerText.text = $"";
            return;
        }

        StartCoroutine(CountdowndamagePotionTimerUI());
    }

    IEnumerator CountdowndamagePotionTimerUI()
    {
        float currentTime = 30f;

        while (currentTime >= 0)
        {
            int seconds = Mathf.FloorToInt(currentTime);

            damagePotionTimerText.text = $"0:{seconds}";

            yield return new WaitForSeconds(1f); // Čekání na jednu sekundu

            currentTime -= 1f;
        }

        UpdatedamagePotiontimerUI(false);
    }

    public void Death() {
        deathScreen.SetActive(true);
    }


    private void SetupPlayerDataUI() {
        GameObject gameStateGO = GameObject.FindGameObjectWithTag("GameState");
        if (gameStateGO == null) { Debug.LogError("No game state object found.", gameObject); return; }
        GameState gameState = gameStateGO.GetComponent<GameState>();
        if (gameState == null) { Debug.LogError("No game state script found.", gameObject); return; }
        PlayerState playerState = gameState.GetComponent<GameState>().GetPlayerState();
        if (gameState == null) { Debug.LogError("No player state found.", gameObject); return;  }

        livesSlider.fillAmount = (float)playerState.GetLives() / (float)playerState.GetMaxLives();

        playerState.livesChangeEvent.AddListener(UpdateLivesUI);
        playerState.moneyChangeEvent.AddListener(UpdateMoneyUI);
        playerState.healthpacksChangeEvent.AddListener(UpdateHealthpacksUI);
        playerState.damagePotionsChangeEvent.AddListener(UpdatedamagePotionUI);
        playerState.speedPotionsChangeEvent.AddListener(UpdatespeedPotionUI);
        playerState.speedPotionstimerChangeEvent.AddListener(UpdatespeedPotiontimerUI);
        playerState.damagePotionstimerChangeEvent.AddListener(UpdatedamagePotiontimerUI);

        UpdateLivesUI(playerState.GetLives(), playerState.GetMaxLives());
        UpdateMoneyUI(playerState.GetMoney());
        UpdateHealthpacksUI(playerState.GetHealthpacks());
        UpdatedamagePotionUI(playerState.GetDamagePotions());
        UpdatespeedPotionUI(playerState.GetSpeedPotions());
        UpdatespeedPotiontimerUI(false);
        UpdatedamagePotiontimerUI(false);
    }
}
