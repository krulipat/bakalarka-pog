using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{

    public GameObject PauseUI;
    private PlayerController playerController;
    bool isPaused = false;


    private void Start()
    {
        GameObject playerGO = GameObject.FindGameObjectWithTag("Player");
        if (playerGO == null) Debug.LogError("Player not found", gameObject);
        playerController = playerGO.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused == true)
                Continue();
            else
                Pause();
        }

    }

    public void Pause()
    {
        isPaused = true;
        playerController.SetMovementEnabled(false);
        PauseUI.SetActive(true);
        Time.timeScale = 0;
    }

    public void Continue()
    {
        isPaused = false;
        playerController.SetMovementEnabled(true);
        PauseUI.SetActive(false);
        Time.timeScale = 1;
    }
}
