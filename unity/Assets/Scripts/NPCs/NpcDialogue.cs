﻿using System;

[Serializable]
public class NpcDialogue {
    public DialogueSO data;
    // Wether this dialogue should only be played once per scene.
    // Dooesn't take into account scene reloading.
    public bool onlyOnce;
    public float lastPlayed;

    public bool HasBeenPlayed() { 
        return lastPlayed != 0;
    }
}
