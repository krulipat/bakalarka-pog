﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(NpcJobs))]
public class Skladnik : MonoBehaviour
{
    [SerializeField] private NpcDialogue defaultDialogue;
    [SerializeField] private NpcDialogue branikBrokenDialogue;
    [SerializeField] private NpcDialogue branikWorkingDialogue;
    [SerializeField] private NpcDialogue breakRoomDialogue;

    [SerializeField] private Transform upperSpawnPoint;
    [SerializeField] private Transform lowerSpawnPoint;
    [SerializeField] private Transform entranceOutside;
    [SerializeField] private Transform boxPile;
    [SerializeField] private Transform microwave;
    [SerializeField] private Transform refridgerator;
    [SerializeField] private Transform armchair;
    [SerializeField] private Transform zidleUStolu;

    [SerializeField] private Navigator navigator;
    [SerializeField] private Animator animator;
    [SerializeField] private Trigger dialogueTrigger;

    private SkladnikState state;
    private bool movingStateBeforeDailogue = false;

    private BranikState branikState;
    private NpcJobs job;
    private MissionsState missionState;

    private void Awake() {
        job = GetComponent<NpcJobs>();
    }

    private void Start() {
        GameState gameState = GameObject.FindGameObjectWithTag("GameState").GetComponent<GameState>();
        missionState = GameObject.FindGameObjectWithTag("GameState").GetComponent<GameState>().GetMissionsState();
        branikState = gameState.GetBranikState();
        job.jobDoneEvent.AddListener(EndState);

        dialogueTrigger.activationEvent.AddListener(Talk);

        job.Teleport(upperSpawnPoint.position);
        ChangeState(SkladnikState.goToCrate);

        /*
        if (missionState.GetMostRecentMission() == "unetice" || missionState.GetMostRecentMission() == "staropramen") {
            job.Teleport(lowerSpawnPoint.position);
            ChangeState(SkladnikState.relaxing);
        } 
        else {
            job.Teleport(lowerSpawnPoint.position);
            ChangeState(SkladnikState.makingFood);
            //branikState.changeEvent.AddListener(ChangeBehaviour);
        }*/

    }

    public void Talk() {
        List<NpcDialogue> availableDialogue = new();

        if (branikState.AllMachinesOwned())
            availableDialogue.Add(branikWorkingDialogue);
        if (!branikState.AllMachinesOwned())
            availableDialogue.Add(branikBrokenDialogue);
        if (state == SkladnikState.relaxing)
            availableDialogue.Add(breakRoomDialogue);

        availableDialogue.Add(defaultDialogue);

        dialogueTrigger.SetIsEnabled(false);
        movingStateBeforeDailogue = animator.GetBool("IsWalking");
        animator.SetBool("IsWalking", false);
        job.dialogueOutputEvent.AddListener(EnableDialogue);
        job.PlayDialogue(availableDialogue);
    }


    public void EndState() {
        switch (state) {
            /*
            case SkladnikState.managingBoxes:
                switch (Random.Range(1, 10)) {
                    case 1: ChangeState(SkladnikState.goingToBreakRoom); break;
                    default: ChangeState(SkladnikState.goToCrate); break; 
                }
                break;
            */
            case SkladnikState.goToCrate:
                ChangeState(SkladnikState.carryTheCrate);
                break;

            case SkladnikState.carryTheCrate:
                ChangeState(SkladnikState.managingBoxes);
                break;

            case SkladnikState.managingBoxes:
                ChangeState(SkladnikState.goToCrate);
                break;

            case SkladnikState.makingFood:
                ChangeState(SkladnikState.relaxing);
                break;

            case SkladnikState.relaxing:
                ChangeState(SkladnikState.makingFood);
                break;
        }
    }

    private void ChangeState(SkladnikState newState) {
        state = newState;

        switch (state) {
            case SkladnikState.goToCrate:
                animator.SetBool("IsWalking", true);
                job.FollowPath(navigator.GetPathFromMe(transform.position, entranceOutside.position));
                break;

            case SkladnikState.carryTheCrate:
                animator.SetBool("IsWalking", true);
                job.FollowPath(navigator.GetPathFromMe(transform.position, boxPile.position));
                break;

            case SkladnikState.managingBoxes:
                animator.SetBool("IsWalking", false);
                job.Wait(3);
                break;

            case SkladnikState.makingFood:
                animator.SetBool("IsWalking", true);
                job.DisplayString("");
                job.FollowPath(navigator.GetPathFromMe(transform.position, refridgerator.position));
                animator.SetBool("IsWalking", false);
                job.Wait(3);
                animator.SetBool("IsWalking", true);
                job.FollowPath(navigator.GetPathFromMe(transform.position, microwave.position));
                animator.SetBool("IsWalking", false);
                job.Wait(10);
                animator.SetBool("IsWalking", true);
                job.FollowPath(navigator.GetPathFromMe(transform.position, zidleUStolu.position));
                animator.SetBool("IsWalking", false);
                job.Wait(50);
                animator.SetBool("IsWalking", true);
                break;

            case SkladnikState.relaxing:
                animator.SetBool("IsWalking", true);
                job.DisplayString("");
                job.FollowPath(navigator.GetPathFromMe(transform.position, armchair.position));
                job.Wait(50);
                break;
        }
    }


    private void EnableDialogue(string output) {
        animator.SetBool("IsWalking", movingStateBeforeDailogue);
        dialogueTrigger.SetIsEnabled(true);
    }

    /// <summary>
    ///     Changes behaviour from doing nothing to doing something. If all machines are owned.
    /// </summary>
    private void ChangeBehaviour() {
        if (branikState.AllMachinesOwned()) {
            ChangeState(SkladnikState.goToCrate);
        }


    }

    private enum SkladnikState {
        managingBoxes,
        goToCrate,
        carryTheCrate,
        makingFood,
        relaxing,
    }

}
