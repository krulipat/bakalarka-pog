using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///     A rectangle defined by its min/max values.
/// </summary>
[Serializable]
public class Rectangle {
    public float minX, maxX, minY, maxY;
}
