﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class PlayerAnimatorOutputManager : MonoBehaviour {

    public UnityEvent<PlayerAnimatorOutput> outputEvent;

    public void GiveOutput(PlayerAnimatorOutput output) {
        outputEvent.Invoke(output);
    }
        
}

public enum PlayerAnimatorOutput {
    lightAttackDamage,
    lightAttackEnd,
    heavyAttackDamage,
    heavyAttackEnd,
    potionEnd,
}