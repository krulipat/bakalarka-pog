using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : DamageableObject {

    [Header("Player Controller")]
    [SerializeField] private Animator avatarAnimator;
    [SerializeField] private PlayerAnimatorOutputManager animatorOutputManager;
    [SerializeField] private AudioManager audioManager;
    [SerializeField] private GameObject itemSlot;

    private bool controlBlocked = false;
    
    private PlayerLightAttack lightAttack;
    private PlayerHeavyAttack heavyAttack;
    private PlayerParry parry;
    private PlayerPotion healthPack;
    private PlayerDamagePotion damagePotion;
    private PlayerSpeedPotion speedPotion;
    private List<PlayerAction>  playerActions;

    private PlayerMovement playerMovement;
    private PlayerState playerState;
    
    private void Awake() {
        lightAttack = GetComponent<PlayerLightAttack>();
        heavyAttack = GetComponent<PlayerHeavyAttack>();
        parry = GetComponent<PlayerParry>();
        healthPack = GetComponent<PlayerPotion>();
        damagePotion = GetComponent<PlayerDamagePotion>();
        speedPotion = GetComponent<PlayerSpeedPotion>();

        playerActions = new List<PlayerAction> { lightAttack, heavyAttack, parry, healthPack, damagePotion, speedPotion};

        playerMovement = GetComponent<PlayerMovement>();
    }

    private void Start() {
        GameState gameState = GameObject.FindGameObjectWithTag("GameState").GetComponent<GameState>();
        if (gameState == null) { Debug.LogError("No game state found."); return; }

        playerState = gameState.GetPlayerState();
    }



    private void Update() {
        /*
        if (Input.GetKeyUp(KeyCode.R)) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        */

        if (Input.GetKeyDown(KeyCode.I) && CanPerformAction()) {
            speedPotion.StartAction();
        }

        if (Input.GetKeyDown(KeyCode.O) && CanPerformAction()) {
            damagePotion.StartAction();
        }

        if (Input.GetKeyDown(KeyCode.P) && CanPerformAction()) {
            healthPack.StartAction();
        }

        if (Input.GetKeyDown(KeyCode.C) && CanPerformAction()) {
            lightAttack.StartAction();
        }

        if (Input.GetKeyDown(KeyCode.X) && CanPerformAction()) {
            heavyAttack.StartAction();
        }

        if (Input.GetKeyDown(KeyCode.Z) && CanPerformAction()) {
            parry.StartAction();
        }
    }

    
    
    public override DamageResponseData RecieveDamage(DamageData damageData) {

        bool parrySuccessful = parry.TryParryDamage(damageData);

        if (!parrySuccessful) {
            InterruptActions();
            avatarAnimator.SetTrigger("damage");
            playerState.ChangeLives(-damageData.damage);
            if (playerState.GetLives() <= 0) {
                // Death
                GameObject.FindAnyObjectByType<UIController>().Death();
            }
        }

        return new DamageResponseData(isSolid, parrySuccessful);
    }

    public void EquipItem(GameObject item) {

        // Destroy any held objects (should always be one or none)
        if (itemSlot.transform.childCount > 0) {
            foreach (Transform child in itemSlot.transform) {
                Destroy(child.gameObject);
            }
        }

        if (item != null) {
            // Can't instantiate item as a child of itemSlot, because of scaling issues
            GameObject newItem = Instantiate(item);
            newItem.transform.parent = itemSlot.transform;

            // Set items position and rotation the same as the itemSlot (so make local values zero)
            newItem.transform.localPosition = Vector3.zero;
            newItem.transform.localRotation = Quaternion.identity;

            // if player is facing left, then the item needs to be flipped
            if (!playerMovement.IsFacingRight()) {
                Vector3 newScale = newItem.transform.localScale;
                newScale.x = -newScale.x;
                newItem.transform.localScale = newScale;
            }
        }
    }

    /// <summary>
    ///     Disable control for player (no movement and no actions).
    /// </summary>
    public void SetControlBlocked(bool blocked) {
        controlBlocked = blocked;
        SetMovementEnabled(!blocked);
    }

    /// <summary>
    ///     Enables/disables player movement.
    /// </summary>
    public void SetMovementEnabled(bool enabled) {
        playerMovement.movementEnabled = enabled;
    }

    public void SetConfinedArea(Rectangle rect) {
        playerMovement.SetConfinedArea(rect);
    }

    public void UnsetConfinedArea() {
        playerMovement.UnsetConfinedArea();
    }

    public Animator GetAvatarAnimator() { return avatarAnimator; }

    public PlayerAnimatorOutputManager GetPlayerAnimatorOutputManager() { return animatorOutputManager; }

    public AudioManager GetAudioManager() { return audioManager; }

    public PlayerState GetPlayerState() { return playerState; }

    /// <summary>
    ///     Stops all actions.
    /// </summary>
    private void InterruptActions() {
        foreach (var action in playerActions) {
            action.InterruptAction();
        }
    }

    /// <summary>
    ///     Checks wether player can perform an action
    /// </summary>
    /// <returns>True if true, false if false</returns>
    private bool CanPerformAction() {
        if (controlBlocked) { 
            return false;
        }
        foreach (var action in playerActions) {
            if (action.IsActive) return false; // tu je problem
        }
        return true;
    }

}
