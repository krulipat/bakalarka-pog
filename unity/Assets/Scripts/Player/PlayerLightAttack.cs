﻿using System.Collections;
using UnityEngine;

public class PlayerLightAttack : PlayerAction {

    [SerializeField] private DamageProducer hitArea;
    [SerializeField] private GameObject lightWeapon;
    
    private PlayerController controller;
    private Animator animator;
    private PlayerAnimatorOutputManager outputManager;

    private void Start() {
        controller = GetComponent<PlayerController>();
        animator = controller.GetAvatarAnimator();
        outputManager = controller.GetPlayerAnimatorOutputManager();

        outputManager.outputEvent.AddListener(Damage);
        outputManager.outputEvent.AddListener(LightAttackEnd);
    }

    public override void StartAction() {
        IsActive = true;
        controller.EquipItem(lightWeapon);
        animator.SetTrigger("lightAttack");
    }

        
    public override void InterruptAction() {
        if (IsActive) {
            Cleanup();
        }
    }

    /// <summary>
    ///     Does the damage.
    /// </summary>
    private void Damage(PlayerAnimatorOutput output) {
        if (IsActive && output == PlayerAnimatorOutput.lightAttackDamage) {
            hitArea.Attack();
        }
    }

    private void LightAttackEnd(PlayerAnimatorOutput output) {
        if (IsActive && output == PlayerAnimatorOutput.lightAttackEnd) {
            Cleanup();
        }
    }

    /// <summary>
    ///     Restores everything to neutral state.
    /// </summary>
    private void Cleanup() {
        IsActive = false;
    }

}
