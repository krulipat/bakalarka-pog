﻿using UnityEngine;

public struct DamageData {

    public int damage;
    public bool isParryable;
    public Vector3 damageOrigin;

    public DamageData(int damage, bool isParryable, Vector3 damageOrigin) {
        this.damage = damage;
        this.isParryable = isParryable;
        this.damageOrigin = damageOrigin;
    }
}
