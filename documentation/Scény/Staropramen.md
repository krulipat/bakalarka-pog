# Staropramen

## Oblasti

### ~~Vesnice~~
- ~~Cesta protínající vesnici~~
- ~~Domečky z plechovek od Staropramen Cool~~

### Cesta podél řeky (skrze vesnici)
- Pod cestou je řeka, na které plují surfaři
- Nad cestou (a na cestě) jsou domečky z plechovky Staropramen Cool (z nich budou vycházet surfaři)
- Nepřátelé:
	- Surfaři - mlátí hráče surfařským prknem

### Pramen
- "Jedna obrazovka" s boss fightem
- Úplně napravo je špunt z pod kterého vytéká Staropramen
- Hráč do něj musí hodněkrát bouchnout, a tím pramen ucpat
- Do toho na něj útočí surfaři