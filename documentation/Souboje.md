# Souboje
## Hráč
- Hráč má 3 vlastnosti:
    - Lehký útok
        - Lze spamovat
        - Prakticky bez cooldownu
        - 3-5 hitů na základního nepřítele
    - Těžký útok
        - Zabije jednou ranou většinu nepřátel
        - Velká oblast poškození, tedy udeří několik nepřátel najednou
        - Velká anticipace (cca 0.5 s)
        - Risk vs. reward
        - Útok se zruší, pokud hráč dostane damage při anticipaci
    - Parry
        - Vykrytí nepřátelského útoku
        - Malé okno pro vykrytí (tedy vyžaduje přesné načasování) (cca 0.1 s)
        - Neguje veškeré poškození
        - Nepřátelé tedy telegraphují své útoky
        - Parry rozhodí nepřítele, který pak chvíli nemůže nic dělat
            - To poskytuje příležitost pro silný útok
    